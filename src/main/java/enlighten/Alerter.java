package enlighten;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * The Alerter analyzes data and outputs alerts
 */
public class Alerter {

	/** The Constant logger. */
	public static final Logger logger = LogManager.getLogger(Alerter.class);

	/** The Constant BATTERY_COMPONENT. */
	public static final String BATTERY_COMPONENT = "BATT";

	/** The Constant THERMOSTAT_COMPONENT. */
	public static final String THERMOSTAT_COMPONENT = "TSTAT";

	/** The Constant BATTERY_NUM_SECONDS. */
	public static final int BATTERY_NUM_SECONDS = 300;

	/** The Constant BATTERY_EVENTS_THRESHOLD. */
	public static final int BATTERY_EVENTS_THRESHOLD = 3;

	/** The Constant THERMOSTAT_NUM_SECONDS. */
	public static final int THERMOSTAT_NUM_SECONDS = 300;

	/** The Constant THERMOSTAT_EVENTS_THRESHOLD. */
	public static final int THERMOSTAT_EVENTS_THRESHOLD = 3;

	public static final String SEVERITY_RED_HIGH = "RED HIGH";

	public static final String SEVERITY_RED_LOW = "RED LOW";

	/** The map. */
	private Map<String, Map<Long, DateTimeList>> map = new HashMap<>();

	private JSONArray output = new JSONArray();

	/**
	 * Instantiates a new alerter.
	 */
	public Alerter() {
	}

	/**
	 * Process bean.
	 *
	 * @param bean the bean
	 * @return true, if successful
	 */
	public boolean processBean(TelemetryBean bean) {
		if (checkForAlerts(bean)) {
			return true;
		}
		return false;
	}

	public String getOutput() {
		return output.toString(4);
	}

	/**
	 * Output alert.
	 *
	 * @param bean the bean
	 * @return the string
	 */
	protected void storeAlert(TelemetryBean bean) {
		DateTimeFormatter fmt = ISODateTimeFormat.dateTime();
		JSONObject obj = new JSONObject();
		obj.put("satelliteId", bean.getSatelliteId());
		obj.put("severity", bean.getSeverity());
		obj.put("component", bean.getComponent());
		obj.put("timestamp", fmt.print(bean.getDateTime()));
		output.put(obj);
	}

	/**
	 * Check for alerts.
	 *
	 * @param bean the bean
	 * @return true, if successful
	 */
	protected boolean checkForAlerts(TelemetryBean bean) {
		if (checkForBatteryVoltageLow(bean)) {
			DateTimeList list = getDateTimeList(bean.getComponent(), bean.getSatelliteId());
			list.add(bean.getDateTime());
			int numEvents = list.numEvents(bean.getDateTime(), BATTERY_NUM_SECONDS);
			if (numEvents >= BATTERY_EVENTS_THRESHOLD) {
				bean.setSeverity(SEVERITY_RED_LOW);
				storeAlert(bean);
				return true;
			}
		}
		if (checkForThermostatHigh(bean)) {
			DateTimeList list = getDateTimeList(bean.getComponent(), bean.getSatelliteId());
			list.add(bean.getDateTime());
			int numEvents = list.numEvents(bean.getDateTime(), THERMOSTAT_NUM_SECONDS);
			if (numEvents >= THERMOSTAT_EVENTS_THRESHOLD) {
				bean.setSeverity(SEVERITY_RED_HIGH);
				storeAlert(bean);
				return true;
			}
		}
		return false;
	}

	/**
	 * Check for battery voltage low.
	 *
	 * @param bean the bean
	 * @return true, if successful
	 */
	protected boolean checkForBatteryVoltageLow(TelemetryBean bean) {
		if (BATTERY_COMPONENT.equals(bean.getComponent())) {
			if (bean.getRawValue() < bean.getRedLowLimit()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Check for thermostat high.
	 *
	 * @param bean the bean
	 * @return true, if successful
	 */
	protected boolean checkForThermostatHigh(TelemetryBean bean) {
		if (THERMOSTAT_COMPONENT.equals(bean.getComponent())) {
			if (bean.getRawValue() > bean.getRedHighLimit()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Gets the limit list.
	 *
	 * @param component the component
	 * @param satId     the sat id
	 * @return the limit list
	 */
	protected DateTimeList getDateTimeList(String component, Long satId) {
		Map<Long, DateTimeList> satMap;
		DateTimeList limitList;

		satMap = map.get(component);
		if (satMap == null) {
			satMap = new HashMap<>();
			map.put(component, satMap);
		}

		limitList = satMap.get(satId);
		if (limitList == null) {
			limitList = new DateTimeList();
			satMap.put(satId, limitList);
		}
		return limitList;

	}

}
