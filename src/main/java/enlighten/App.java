package enlighten;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Iterator;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;

// TODO: Auto-generated Javadoc
/**
 * Main App for Paging Mission Control.
 */
public class App {

	/** The alerter. */
	private static Alerter alerter = new Alerter();

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		String filename = args[0];
		App app = new App();
		try {
			Iterator<TelemetryBean> beans = app.parseFile(filename);
			while (beans.hasNext()) {
				TelemetryBean bean = beans.next();
				app.processBean(bean);
			}
			app.outputAlerts();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Process bean.
	 *
	 * @param bean the bean
	 */
	protected void processBean(TelemetryBean bean) {
		alerter.processBean(bean);
	}

	protected void outputAlerts() {
		System.out.println(alerter.getOutput());
	}

	/**
	 * Instantiates a new app.
	 */
	protected App() {
	}

	/**
	 * Parses the file.
	 *
	 * @param filename the filename
	 * @return the iterator
	 * @throws IllegalStateException the illegal state exception
	 * @throws FileNotFoundException the file not found exception
	 */
	protected Iterator<TelemetryBean> parseFile(String filename) throws IllegalStateException, FileNotFoundException {

		CsvToBean<TelemetryBean> csvToBean = new CsvToBeanBuilder<TelemetryBean>(new FileReader(filename))
				.withType(TelemetryBean.class).withSeparator('|').build();
		return csvToBean.iterator();

	}

}
