package enlighten;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Duration;

/**
 * The Class LimitList stores the DateTime of events that were outside
 * parameters.
 */
public class DateTimeList {

	/** The Constant logger. */
	public static final Logger logger = LogManager.getLogger(DateTimeList.class);

	/** The list. */
	// List of events DateTime that fell outside list
	private List<DateTime> list = new ArrayList<>();

	/**
	 * Instantiates a new DateTime list.
	 */
	public DateTimeList() {
	}

	/**
	 * Adds the object.
	 *
	 * @param dt the dt
	 * @return true, if successful
	 */
	public boolean add(DateTime dt) {
		return list.add(dt);
	}

	/**
	 * Contains the object.
	 *
	 * @param dt the dt
	 * @return true, if successful
	 */
	public boolean contains(DateTime dt) {
		return list.contains(dt);

	}

	/**
	 * Number of events in list.
	 *
	 * @param dt      the dt
	 * @param seconds the seconds
	 * @return the int
	 */
	public int numEvents(DateTime dt, int seconds) {
		int matches = 0;
		for (DateTime item : list) {
			Duration duration = new Duration(item, dt);
			logger.debug("Seconds: " + duration.getStandardSeconds());
			if (duration.getStandardSeconds() <= seconds) {
				++matches;
			}
		}
		logger.debug("Matches: " + matches);
		return matches;
	}

	/**
	 * Size of the list.
	 *
	 * @return the size as int
	 */
	protected int size() {
		return list.size();
	}

}
