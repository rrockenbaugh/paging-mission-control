package enlighten;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.opencsv.bean.CsvBindByPosition;

// TODO: Auto-generated Javadoc
/**
 * The Class TelemetryBean for Telemetry Data.
 */
public class TelemetryBean {
	
	public final static Logger logger = LogManager.getLogger(TelemetryBean.class);
	
	public final static String DATE_TIME_FORMAT = "yyyyMMdd HH:mm:ss.SSS";
	
	@CsvBindByPosition(position = 0)
	private String timestamp;
	
	@CsvBindByPosition(position = 1)
	private Long satelliteId;

	@CsvBindByPosition(position = 2)
	private Double redHighLimit;	

	@CsvBindByPosition(position = 3)
	private Double yellowHighLimit;
	
	@CsvBindByPosition(position = 4)
	private Double yellowLowLimit;
	
	@CsvBindByPosition(position = 5)
	private Double redLowLimit;
	
	@CsvBindByPosition(position = 6)
	private Double rawValue;

	@CsvBindByPosition(position = 7)
	private String component;
	
	private String severity;

	public String getSeverity() {
		return severity;
	}

	public void setSeverity(String severity) {
		this.severity = severity;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public Long getSatelliteId() {
		return satelliteId;
	}

	public void setSatelliteId(Long satelliteId) {
		this.satelliteId = satelliteId;
	}

	public Double getRedHighLimit() {
		return redHighLimit;
	}

	public void setRedHighLimit(Double redHighLimit) {
		this.redHighLimit = redHighLimit;
	}

	public Double getYellowHighLimit() {
		return yellowHighLimit;
	}

	public void setYellowHighLimit(Double yellowHighLimit) {
		this.yellowHighLimit = yellowHighLimit;
	}

	public Double getYellowLowLimit() {
		return yellowLowLimit;
	}

	public void setYellowLowLimit(Double yellowLowLimit) {
		this.yellowLowLimit = yellowLowLimit;
	}

	public Double getRedLowLimit() {
		return redLowLimit;
	}

	public void setRedLowLimit(Double redLowLimit) {
		this.redLowLimit = redLowLimit;
	}

	public Double getRawValue() {
		return rawValue;
	}

	public void setRawValue(Double rawValue) {
		this.rawValue = rawValue;
	}

	public String getComponent() {
		return component;
	}

	public void setComponent(String component) {
		this.component = component;
	}
	
	public DateTime getDateTime() {
		return convertBeanTimestamp(this.timestamp);
	}
	
	/**
	 * Tries to convert a String to DateTime Object.
	 *
	 * @param text the datetime text to convert
	 * @return DateTime Object, null if conversion fails
	 */
	protected static DateTime convertBeanTimestamp(String text) {
		DateTimeFormatter formatter = DateTimeFormat.forPattern(DATE_TIME_FORMAT).withZoneUTC();
		DateTime dt = null;
		try {
			dt = formatter.parseDateTime(text);
		} catch (Exception e) {
			//Just log exception
			logger.info(e.getMessage());
		}
		return dt;
	}
	
}
