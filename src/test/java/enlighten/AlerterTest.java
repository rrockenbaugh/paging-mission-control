package enlighten;

import static org.junit.Assert.*;

import org.joda.time.DateTime;
import org.junit.Test;

public class AlerterTest {

	@Test
	public void testProcessBean() {
		TelemetryBean bean = new TelemetryBean();

		Alerter alerter = new Alerter();
		alerter.processBean(bean);
	}

	@Test
	public void testCheckForBatteryVoltageLow() {
		TelemetryBean bean = new TelemetryBean();
		Alerter alerter = new Alerter();
		boolean flag = alerter.checkForBatteryVoltageLow(bean);
		assertFalse(flag);

		bean.setComponent(Alerter.BATTERY_COMPONENT);
		bean.setRedLowLimit(10.0);
		bean.setRawValue(10.0001);
		flag = alerter.checkForBatteryVoltageLow(bean);
		assertFalse(flag);

		bean.setRedLowLimit(10.0);
		bean.setRawValue(9.9999);
		flag = alerter.checkForBatteryVoltageLow(bean);
		assertTrue(flag);

		bean.setComponent(Alerter.THERMOSTAT_COMPONENT);
		bean.setRawValue(9.9999);
		flag = alerter.checkForBatteryVoltageLow(bean);
		assertFalse(flag);
	}

	@Test
	public void testCheckForThermostatHigh() {
		TelemetryBean bean = new TelemetryBean();
		Alerter alerter = new Alerter();
		boolean flag = alerter.checkForThermostatHigh(bean);
		assertFalse(flag);

		bean.setComponent(Alerter.THERMOSTAT_COMPONENT);
		bean.setRedHighLimit(10.0);
		bean.setRawValue(10.0001);
		flag = alerter.checkForThermostatHigh(bean);
		assertTrue(flag);

		bean.setRedHighLimit(10.0);
		bean.setRawValue(9.9999);
		flag = alerter.checkForThermostatHigh(bean);
		assertFalse(flag);
	}

	@Test
	public void testGetLimitList() {
		boolean flag = false;
		String component = Alerter.BATTERY_COMPONENT;
		Long satId = 1L;
		Alerter alerter = new Alerter();
		DateTimeList list = alerter.getDateTimeList(component, satId);
		assertNotNull(list);
		assertEquals(0, list.size());
		assertFalse(list.contains(new DateTime()));

		String timestamp = "20180101 23:01:05.001";
		TelemetryBean bean = new TelemetryBean();
		bean.setComponent(component);
		bean.setSatelliteId(satId);
		bean.setRawValue(10.10);
		bean.setRedLowLimit(14.0);
		bean.setTimestamp(timestamp);
		alerter.processBean(bean);
		list = alerter.getDateTimeList(component, satId);
		assertEquals(1, list.size());
		assertTrue(list.contains(bean.getDateTime()));

		timestamp = "20180101 23:01:06.001";
		bean.setTimestamp(timestamp);
		flag = alerter.processBean(bean);
		assertFalse(flag);
		list = alerter.getDateTimeList(component, satId);
		assertEquals(2, list.size());
		assertTrue(list.contains(bean.getDateTime()));

		timestamp = "20180101 23:01:07.001";
		bean.setTimestamp(timestamp);
		flag = alerter.processBean(bean);
		assertTrue(flag);
		list = alerter.getDateTimeList(component, satId);
		assertEquals(3, list.size());
		assertTrue(list.contains(bean.getDateTime()));

	}

}
