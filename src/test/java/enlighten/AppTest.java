package enlighten;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;

import org.junit.Test;

public class AppTest {

	@Test
	public void testParseFile() {
		String filename = "src/test/resources/enlighten/sampledata.txt";
		App app = new App();
		try {
			Iterator<TelemetryBean> list = app.parseFile(filename);
			assertEquals(list.next().getRawValue(), 99.9, 0);
			assertNotNull(list);
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testParseFileAndProcess() {
		String filename = "src/test/resources/enlighten/sampledata.txt";
		App app = new App();
		try {
			Iterator<TelemetryBean> list = app.parseFile(filename);
			assertEquals(list.next().getRawValue(), 99.9, 0);
			assertNotNull(list);
			while (list.hasNext()) {
				app.processBean(list.next());
			}
			app.outputAlerts();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
