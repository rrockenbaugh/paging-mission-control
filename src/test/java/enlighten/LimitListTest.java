package enlighten;

import static org.junit.Assert.*;

import org.joda.time.DateTime;
import org.junit.Test;

public class LimitListTest {

	@Test
	public void testAdd() {
		DateTimeList list = new DateTimeList();
		DateTime dt = new DateTime();
		boolean flag = list.add(dt);
		assertTrue(flag);
		assertTrue(list.contains(dt));
	}

	@Test
	public void testNumMatches() {
		DateTimeList list = new DateTimeList();
		DateTime dt1 = new DateTime();
		DateTime dt2 = dt1.plusSeconds(60);
		list.add(dt1);
		int numMatches = list.numEvents(dt2, 300);
		assertEquals(1, numMatches);

		list.add(dt2);
		DateTime dt3 = dt2.plusSeconds(60);
		numMatches = list.numEvents(dt3, 300);
		assertEquals(2, numMatches);

		list.add(dt3);
		DateTime dt4 = dt3.plusSeconds(600);
		numMatches = list.numEvents(dt4, 300);
		assertEquals(0, numMatches);

	}

}
