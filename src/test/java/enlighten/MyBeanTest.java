package enlighten;

import static org.junit.Assert.*;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Test;

public class MyBeanTest {

	@Test
	public void testConvertBeanTimestamp() {
		String validDate = "20180101 23:01:05.001";
		DateTime dt = new DateTime(2018, 1, 1, 23, 1, 5, 1, DateTimeZone.UTC);
		DateTime conversion = TelemetryBean.convertBeanTimestamp(validDate);
		assertEquals(conversion, dt);

		// Expect Null for invalid dates
		String invalidDate = "20180231 23:01:05.001";
		DateTime dt2 = new DateTime(2018, 1, 1, 23, 1, 5, 1, DateTimeZone.UTC);
		DateTime conversion2 = TelemetryBean.convertBeanTimestamp(invalidDate);
		assertNull(conversion2);
	}

}
